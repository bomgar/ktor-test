package bomgar.email.routes

import bomgar.email.Components
import bomgar.email.customer.Customer
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*


fun Route.customerByIdRoute(components: Components) {
    get("/customer/{id}") {
        val customer = components.customerService.getCustomer(call.parameters["id"]!!)
        if(customer == null) {
            call.respondText("Unknown customer", status = HttpStatusCode.NotFound)
        } else {
            call.respond(Customer(name="Hugo"))
        }
    }
}
