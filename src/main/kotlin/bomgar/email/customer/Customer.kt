package bomgar.email.customer

import kotlinx.serialization.Serializable

@Serializable
data class Customer(val name: String)
