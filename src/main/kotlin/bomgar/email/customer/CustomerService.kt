package bomgar.email.customer

class CustomerService {

    fun getCustomer(id: String): Customer? {
        return when (id) {
            "1" -> Customer("Hugo")
            else -> null
        }
    }
}
