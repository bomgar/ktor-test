package bomgar.email

import bomgar.email.customer.CustomerService


open class Components {
    open val customerService: CustomerService by lazy {
        CustomerService()
    }
}
