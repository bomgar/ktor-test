package bomgar.email

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.serialization.*

fun Application.configureFormats() {
    install(ContentNegotiation) {
        json()
    }
}


