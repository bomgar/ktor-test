package bomgar.email.plugins

import bomgar.email.Components
import bomgar.email.routes.customerByIdRoute
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*

fun Application.configureRouting(components: Components) {

    routing {
        customerByIdRoute(components)
        get("/") {
            call.respondText("Hello World!")
        }
    }
}


