package bomgar.email

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import bomgar.email.plugins.*

fun main() {
    val components: Components = Components()
    embeddedServer(Netty, port = 8080, host = "0.0.0.0") {
        configureMonitoring()
        configureRouting(components)
        configureFormats()
    }.start(wait = true)
}
