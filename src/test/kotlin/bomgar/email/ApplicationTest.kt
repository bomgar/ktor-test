package bomgar.email

import bomgar.email.plugins.configureRouting
import io.kotest.assertions.json.shouldContainJsonKeyValue
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.server.testing.*

class ApplicationTest : FunSpec() {

    private fun Application.initTestApp() {
        // TODO overwrite with mocks here
        val components = Components()
        configureRouting(components);
        configureFormats()
    }

    init {
        test("root") {
            withTestApplication({ initTestApp() }) {
                handleRequest(HttpMethod.Get, "/").apply {
                    response.status() shouldBe HttpStatusCode.OK
                    response.content shouldBe "Hello World!"
                }
            }
        }

        test("customer") {
            withTestApplication({ initTestApp() }) {
                handleRequest(HttpMethod.Get, "/customer/1"){
                    addHeader("Accept", "application/json")

                }.apply {
                    response.status() shouldBe HttpStatusCode.OK
                    response.content.shouldContainJsonKeyValue("$.name", "Hugo")
                }
            }
        }

        test("customer not found") {
            withTestApplication({ initTestApp() }) {
                handleRequest(HttpMethod.Get, "/customer/10"){
                    addHeader("Accept", "application/json")

                }.apply {
                    response.status() shouldBe HttpStatusCode.NotFound
                }
            }
        }
    }
}
